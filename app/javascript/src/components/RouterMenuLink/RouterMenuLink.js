import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  Route,
  Link
} from 'react-router-dom'
import { Menu } from 'semantic-ui-react'

export default class RouterMenuLink extends Component {
  static propTypes = {
    to: PropTypes.string.isRequired,
    activeOnlyWhenExact: PropTypes.bool
  }

  static defaultProps = {
    activeOnlyWhenExact: false
  }

  render() {
    const { name, to, activeOnlyWhenExact, children } = this.props

    return (
      <Route path={to} exact={activeOnlyWhenExact} children={({ match }) => (
        <Menu.Item active={!!match} as={Link} to={to}>
          {children}
        </Menu.Item>
      )}/>
    )
  }
}
