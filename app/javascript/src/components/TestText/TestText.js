import React, { Component } from 'react'
import PropTypes from 'prop-types'

import styles from './TestText.scss'

export default class TestText extends Component {
  static propTypes = {
    text: PropTypes.string
  }

  static defaultProps = {
    text: "omg"
  }

  render() {
    return (
      <div className={styles.blah}>
        {this.props.text}
      </div>
    )
  }
}
