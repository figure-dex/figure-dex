import React, { Component, PropTypes } from 'react'
import { Container } from 'semantic-ui-react'

export default class Home extends Component {
  render() {
    return (
      <Container>
        Home
      </Container>
    )
  }
}
