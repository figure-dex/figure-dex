import React, { Component, PropTypes } from 'react'
import { Container } from 'semantic-ui-react'
import { Route, Link } from 'react-router-dom'

import NewEntry from '../NewEntry/NewEntry'

export default class Database extends Component {
  render() {
    return (
      <Container>
        <Link to="/database/new">New Entry</Link>
        <Route path="/database/new" component={NewEntry}/>
      </Container>
    )
  }
}
