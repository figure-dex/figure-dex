import React, { Component, PropTypes } from 'react'
import { Provider } from 'react-redux'
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom'
import { Menu, Container } from 'semantic-ui-react'

import Home from '../Home/Home'
import Database from '../Database/Database'

import RouterMenuLink from '../../components/RouterMenuLink/RouterMenuLink'

export default class App extends Component {
  static propTypes = {
    store: PropTypes.object.isRequired,
  }

  render() {
    const { store } = this.props
    return (
      <Provider store={store}>
        <Router>
          <div>
            <Menu size='huge'>
              <Container>
                <RouterMenuLink activeOnlyWhenExact to='/'>Home</RouterMenuLink>
                <RouterMenuLink to='/database'>Database</RouterMenuLink>
              </Container>              
            </Menu>
            <div>
              <Route exact path="/" component={Home}/>
              <Route path="/database" component={Database}/>
            </div>
          </div>
        </Router>
      </Provider>
    )
  }
}