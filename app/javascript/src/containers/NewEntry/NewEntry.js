import React, { Component, PropTypes } from 'react'
import { Container, Form } from 'semantic-ui-react'

export default class NewEntry extends Component {
  render() {
    return (
      <Container>
        New Entry!!!
        <Form>
          <Form.Input label='Name' placeholder='Name'/>
          <Form.Button>Submit</Form.Button>
        </Form>
      </Container>
    )
  }
}
