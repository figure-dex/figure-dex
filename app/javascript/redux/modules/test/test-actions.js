import { SET_TEST } from './test-action-types'

export const setTest = test => ({
  type: SET_TEST,
  payload: test
})
