import { SET_TEST } from 'redux/modules/test/test-action-types'

const test = (state = {}, action = {}) => {
  const { type, payload } = action

  switch (type) {
    case SET_TEST:
      return payload

    default:
      return state
  }
}

export default test
