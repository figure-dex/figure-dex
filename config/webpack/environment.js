const { environment } = require('@rails/webpacker')

environment.loaders.set('style', {
  test: /\.scss$/,
  loaders: [
    'style-loader',
    'css-loader?modules&sourceMap&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]',
    'sass-loader'
  ]
})

module.exports = environment
