Rails.application.routes.draw do
  root 'webapp#index'

  get '*path', to: 'webapp#index'
end
